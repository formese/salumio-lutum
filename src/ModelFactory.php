<?php

namespace SalumIo\Lutum;

class ModelFactory
{
    /**
     * @var array
     */
    protected $models = [];

    /**
     * @var string
     */
    protected $ns;

    /**
     * @var callable
     */
    protected $resolver;

    /**
     * @param callable $resolver
     * @param string $ns Namespace models are stored under
     */
    public function __construct(callable $resolver, $ns)
    {
        $this->resolver = $resolver;
        $this->ns = $ns;
    }

    /**
     * @param string $key Model to load
     * @return mixed
     */
    public function __get($key)
    {
        if (isset($this->models[$key])) {
            return $this->models[$key];
        }
        $parts = explode('_', $key);
        $class = [];
        $class[] = $this->ns;
        $model = array_pop($parts);
        if (count($parts)) {
            foreach ($parts as $ns) {
                $class[] = ucfirst($ns);
            }
        }
        $class[] = 'Model';
        $class[] = ucfirst($model);

        $class = implode('\\', $class);

        $resolver = $this->resolver;
        $this->models[$key] = $resolver($class);

        return $this->models[$key];
    }
}
